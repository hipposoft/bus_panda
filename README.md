# Bus Panda

Realtime bus information for Wellington, New Zealand, [available from the Apple App Store](https://itunes.apple.com/nz/app/bus-panda/id1071652095).

## XCode project

This entire repository represents the set of resources including the project folder that should be opened in XCode.

## Third party components

Many sincere thanks to the following pieces of software, without which this project would have been very much harder to complete:

* https://github.com/nolanw/HTMLReader
* https://github.com/MortimerGoro/MGSwipeTableCell
* https://github.com/intuit/LocationManager
